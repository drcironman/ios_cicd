//
//  demo456App.swift
//  demo456
//
//  Created by DRC-E4E on 2020/11/26.
//

import SwiftUI

@main
struct demo456App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
